#include <inttypes.h>

uint16_t fun(uint16_t p) {

  const uint16_t mask = 0b1111110000000111;
  const uint16_t fill = 0b0000001011010000;

  p &= mask;
  p |= fill;

  return p;
}
