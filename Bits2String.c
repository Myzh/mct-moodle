#include <inttypes.h>

void Bits2String(uint16_t bits, char* string_ptr) {

  for (int i = 0; i <= 15; i++) {
    if ((1 << i) & bits) {
      string_ptr[15 - i] = '1';
    } else {
      string_ptr[15 - i] = '0';
    }
  }

  string_ptr[16] = '\0';

  return;
}
