#include <inttypes.h>
#include <stdio.h>
#include "extra/fonts.h"

void LCD_WriteLetter(uint8_t c) {

  // use a pointer to the font array as an index
  for (uint8_t* i = ((uint8_t*) console_font_12x16 + (32 * (uint16_t) c)); i < console_font_12x16 + (32 * (uint16_t) c) + 32; i++) {

    // iterate through the bits of 2 chars (1 lin = 12 bits)
    for (uint8_t j = 15; j > (15 - 12); j--) {

      // increment the pointer if we have reached the second char
      if (j == 7) {
        i++;
      }

      // apply a bit mask to the char to read the bit we are looking for
      if ((((uint8_t) 1) << (j % 8)) & *i) {

        // print a star if the bit is 1
        printf("*");
      } else {

        // print a space if the bit is 0
        printf(" ");
      }   
    }

    // we have reached the end of the line
    printf("\n");
  } 
}
