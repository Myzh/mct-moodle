#include <inttypes.h>

uint8_t checksum(const uint8_t bytes[], uint8_t n, uint8_t id) {

  // iterate through the bytes
  for (int i = 0; i < n; i++) {

    // add the bytes value onto the current sum (we use the id int as the sum)
    uint8_t tmp = id + bytes[i];

    // check if we had an overflow
    if (tmp < id) {

      // an overflow accured, add a one as well
      id = tmp + 1;
    } else {

      // no overflow accured, just assign to the sum
      id = tmp;
    }
  }
  
  // return the bitflipped sum
  return ~id;
}
