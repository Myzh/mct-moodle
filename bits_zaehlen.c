#include <inttypes.h>

int bits_zaehlen(uint32_t eingabe) {

  int ret = 0;

  for (uint8_t i = 0; i < 32; i++) {
    if ((1 << i) & eingabe) {
      ret++;
    }
  }

  return ret;
}
