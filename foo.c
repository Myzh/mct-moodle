#include <inttypes.h>

uint32_t foo(uint32_t p) {
    
  const uint32_t group1 = p & 0x0F0F0F0F;
  const uint32_t group2 = p & 0xF0F0F0F0;

  return ((group1 << 4) | (group2 >> 4));
}
