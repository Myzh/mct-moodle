# Makefile

CMP = gcc
CFLAGS += -Wall
CFLAGS += -O3

all: testbench

clean:
	rm -rf *.out

testbench: testbench.c
	$(CMP) $(CFLAGS) -o $@.out $@.c
