URL="https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2?revision=05382cca-1721-44e1-ae19-1e7c3dc96118&la=en&hash=D7C9D18FCA2DD9F894FD9F3C3DC9228498FA281A"
DIR="gcc-arm"

wget -O .tmp $URL
mkdir -p .dir
tar -xf .tmp -C .dir
rm .tmp

mv .dir/* $DIR
rm -r .dir
