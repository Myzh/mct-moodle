#include <inttypes.h>

struct regGPIO {
  uint16_t ODR;
};

struct regGPIO *GPIOD;
struct regGPIO *GPIOE;


// general function to copy sections of bitfields
static inline void dataToReg(uint16_t *data, uint16_t *reg, uint8_t fromData, uint8_t fromReg, uint8_t size) {

  // generate bitmasks (dataMask to be applied to the input data, regMask to be applied to the output data)
  const uint16_t dataMask = (((1 << size) - 1) << fromData);
  const uint16_t regMask = ~(((1 << size) - 1) << fromReg);

  uint16_t tmp;

  // decide on the shift direction
  if (fromData > fromReg) { 
    tmp = ((*data & dataMask) >> (fromData - fromReg));
  } else {
    tmp = ((*data & dataMask) << (fromReg - fromData));
  }

  // write to the output register
  *reg = (*reg & regMask) | tmp;
}

void LCD_Output16BitWord(uint16_t data) {

  // call the helper function for all copy operations
  dataToReg(&data, &GPIOD->ODR, 0, 14, 2);
  dataToReg(&data, &GPIOD->ODR, 2, 0, 2);
  dataToReg(&data, &GPIOD->ODR, 13, 8, 3);
  dataToReg(&data, &GPIOE->ODR, 4, 7, 9);
}
