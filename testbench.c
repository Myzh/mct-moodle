// includes
#include <stdio.h>
#include <stdlib.h>


// local test file
#include "bits_zaehlen.c"
#include "fun.c"
#include "foo.c"
#include "Bits2String.c"
#include "LCD_Output16BitWord.c"
#include "checksum.c"
#include "Buchstabe.c"

int main() {

  // bits_zaehlen
  printf("bits_zaehlen\n");

  printf("%d\n", bits_zaehlen(5));
  printf("%d\n", bits_zaehlen(0x2A));
  printf("%d\n", bits_zaehlen(0x5FF5));

  printf("\n");

  // fun
  printf("fun\n");

  printf("%d\n", fun(0));
  printf("%d\n", fun(65535));

  printf("\n");

  // foo
  printf("foo\n");

  printf("0x%X\n", foo(0x12345678));

  printf("\n");

  // Bits2String
  char str[20];
  printf("Bits2String\n");

  Bits2String(0, str);
  printf("%s\n", str);
  Bits2String(0xDEAD, str);
  printf("%s\n", str);

  printf("\n");
  
  // LCD_Output16BitWord
  GPIOD = malloc(sizeof(struct regGPIO));
  GPIOE = malloc(sizeof(struct regGPIO));

  printf("LCD_Output16BitWord\n");

  GPIOD->ODR = 0;
  GPIOE->ODR = 0;
  LCD_Output16BitWord(0);
  printf("0x%04X,0x%04X\n", GPIOD->ODR, GPIOE->ODR);

  GPIOD->ODR = 0;
  GPIOE->ODR = 0;
  LCD_Output16BitWord(0xFFFF);
  printf("0x%04X,0x%04X\n", GPIOD->ODR, GPIOE->ODR);

  GPIOD->ODR = 0xFFFF;
  GPIOE->ODR = 0xFFFF;
  LCD_Output16BitWord(0);
  printf("0x%04X,0x%04X\n", GPIOD->ODR, GPIOE->ODR);

  GPIOD->ODR = 0;
  GPIOE->ODR = 0;
  LCD_Output16BitWord(4);
  printf("0x%04X,0x%04X\n", GPIOD->ODR, GPIOE->ODR);

  GPIOD->ODR = 0xFFFF;
  GPIOE->ODR = 0xFFFF;
  LCD_Output16BitWord(4);
  printf("0x%04X,0x%04X\n", GPIOD->ODR, GPIOE->ODR);

  GPIOD->ODR = 0;
  GPIOE->ODR = 0;
  LCD_Output16BitWord(0x0100);
  printf("0x%04X,0x%04X\n", GPIOD->ODR, GPIOE->ODR);

  GPIOD->ODR = 0xFFFF;
  GPIOE->ODR = 0xFFFF;
  LCD_Output16BitWord(0x0100);
  printf("0x%04X,0x%04X\n", GPIOD->ODR, GPIOE->ODR);

  printf("\n");
  
  // checksum

  printf("checksum\n");

  uint8_t bytes1[] = {0, 0};
  printf("0x%02X\n", checksum(bytes1, 2, 0));

  uint8_t bytes2[] = {0x80, 0x80, 0x3F, 0xC1};
  printf("0x%02X\n", checksum(bytes2, 4, 0xEC));

  // Buchstabe

  printf("Buchstabe\n");
  LCD_WriteLetter('\0');
  LCD_WriteLetter('B');
  LCD_WriteLetter('x');
  LCD_WriteLetter('8');
  
  return 0;
}
